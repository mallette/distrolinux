#!/usr/bin/env bash

# Création et mise à jour Linux Ubuntu pour CEMEA AN

mkdir paquets
cd paquets
wget https://github.com/torakiki/pdfsam/releases/download/v4.1.4/pdfsam_4.1.4-1_amd64.deb
cd ..

# Depot OpenFortigui

add-apt-repository -y ppa:phoerious/keepassxc # dépot keepassxc
add-apt-repository -y ppa:nextcloud-devs/client # depot nextcloud-client
add-apt-repository -y ppa:linuxuprising/shutter # dépot shutter

dpkg -i paquets/*.deb
apt-get -y install --fix-broken

apt-get -y purge firefox-locale-de firefox-locale-en firefox-locale-es firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans thunderbird-locale-de thunderbird-locale-en thunderbird-locale-en-gb thunderbird-locale-en-us thunderbird-locale-es thunderbird-locale-es-ar thunderbird-locale-es-es thunderbird-locale-it thunderbird-locale-pt thunderbird-locale-pt-br thunderbird-locale-pt-pt thunderbird-locale-ru thunderbird-locale-zh-cn thunderbird-locale-zh-hans thunderbird-locale-zh-hant thunderbird-locale-zh-tw hunspell-* hyphen-* mythes-* libreoffice-help-de libreoffice-help-en-gb libreoffice-help-en-us libreoffice-help-es libreoffice-help-it libreoffice-help-pt libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw  libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw
apt-get -y purge fonts-tlwg-* fonts-lohit-* fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak-* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-takao-pgothic fonts-takao-gothic fonts-telu* fonts-khmeros-core* fonts-tibetan-machine* fonts-deva-extra* fonts-kacst* fonts-kacst-one* fonts-kalapi* fonts-nakula* fonts-nanum* fonts-navilu* fonts-noto* fonts-noto-cjk* fonts-noto-hinted* fonts-noto-mono* fonts-noto-unhinted* fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lao fonts-lklug-sinhala fonts-wqy-microhei fonts-beng-extra
apt-get -y update
apt-get -y install git curl mythes-fr hyphen-fr hunspell-fr wfrench manpages-fr manpages-fr-extra doc-linux-fr-text p7zip geany geany-plugins vim grsync meld keepassxc bleachbit thunderbird thunderbird-locale-fr firefox firefox-locale-fr pidgin pwgen pass typecatcher gparted synaptic whois pidgin freeplane scribus dia dia-shapes xournal pinta gimp gimp-help-fr gthumb inkscape shutter audacity audacious kdenlive soundconverter easytag handbrake ffmpeg gnome-games gnome-chess klavaro remmina remmina-plugin-rdp remmina-plugin-vnc fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine gsfonts-x11 fonts-materialdesignicons-webfont fonts-arkpandora fonts-entypo libreoffice-style-human libreoffice-style-oxygen libreoffice-style-elementary libreoffice-style-sifr libreoffice-style-hicontrast tango-icon-theme ttf-mscorefonts-installer grsync timeshift nextcloud-client vlc gnome-tweaks gnome-shell gnome-shell-extensions  chromium-browser pavucontrol calibre p7zip-full p7zip-rar 

# Paquets non-libres
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections
apt-get install ttf-mscorefonts-installer
export DEBIAN_FRONTEND=noninteractive
apt-get -yq install ubuntu-restricted-extras libdvd-pkg gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad
bash /usr/lib/libdvd-pkg/b-i_libdvdcss.sh
# au lieu de dpkg-reconfigure --frontend=noninteractive libdvd-pkg

# Mise à jour & Nettoyage
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install -y --fix-broken


#suppression langue en
apt-get --yes purge language-pack-en language-pack-en-base language-pack-gnome-en language-pack-gnome-en-base

apt-get -y update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install -y --fix-broken
apt-get --yes clean
apt-get --yes autoremove

# Post install

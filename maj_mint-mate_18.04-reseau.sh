#!/usr/bin/env bash
# Création et mise à jour Mint MATE pour réseau CEMEA

if [ "$(whoami)" != "root" ]; then
	echo "Désolé, vous n'êtes pas roooooot !"
	exit 1
fi

apt-get -y update

mkdir -p tmpdeb && cd tmpdeb

if [ ! -f "master-pdf-editor-5.3.16_qt5.amd64.deb" ]; then
	wget https://code-industry.net/public/master-pdf-editor-5.3.16_qt5.amd64.deb
fi
if [ ! -f "pdfsam_4.0.1-1_all.deb" ]; then
	wget https://github.com/torakiki/pdfsam/releases/download/v4.0.4/pdfsam_4.0.4-1_amd64.deb
fi
if [ ! -f "Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack" ]; then
	wget https://download.virtualbox.org/virtualbox/6.0.4/Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack
fi

dpkg -i ./*.deb
rm -rf ./*.deb*

cd ..

apt-get -y install --fix-broken

add-apt-repository -y ppa:phoerious/keepassxc # dépot keepassxc
add-apt-repository -y ppa:libreoffice/ppa # depot stable Libreoffice
add-apt-repository -y ppa:teejee2008/ppa # dépot timeshift
add-apt-repository -y ppa:nextcloud-devs/client #depot Nextcloud-client
add-apt-repository -y ppa:ubuntuhandbook1/shutter # depot Shutter

echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bionic contrib" > /etc/apt/sources.list.d/virtualbox.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -


apt-get -y purge firefox-locale-de firefox-locale-en firefox-locale-es firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans thunderbird-locale-de thunderbird-locale-en thunderbird-locale-en-gb thunderbird-locale-en-us thunderbird-locale-es thunderbird-locale-es-ar thunderbird-locale-es-es thunderbird-locale-it thunderbird-locale-pt thunderbird-locale-pt-br thunderbird-locale-pt-pt thunderbird-locale-ru thunderbird-locale-zh-cn thunderbird-locale-zh-hans thunderbird-locale-zh-hant thunderbird-locale-zh-tw hunspell-* hyphen-* mythes-* libreoffice-help-de libreoffice-help-en-gb libreoffice-help-en-us libreoffice-help-es libreoffice-help-it libreoffice-help-pt libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw  libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw
apt-get -y purge fonts-tlwg-* fonts-lohit-* fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak-* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-takao-pgothic fonts-takao-gothic fonts-telu* fonts-khmeros-core* fonts-tibetan-machine* fonts-deva-extra* fonts-kacst* fonts-kacst-one* fonts-kalapi* fonts-nakula* fonts-nanum* fonts-navilu* fonts-noto* fonts-noto-cjk* fonts-noto-hinted* fonts-noto-mono* fonts-noto-unhinted* fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lao fonts-lklug-sinhala fonts-wqy-microhei fonts-beng-extra
apt-get -y update
apt-get  -y install git curl mythes-fr hunspell hunspell-fr wfrench manpages-fr manpages-fr-extra doc-linux-fr-text fortunes-fr p7zip geany geany-plugins fslint vim grsync meld tomboy keepassxc bleachbit thunderbird thunderbird-locale-fr firefox firefox-locale-fr pidgin pwgen pass typecatcher gparted synaptic whois sysinfo pidgin freeplane scribus dia dia-shapes xournal pinta gimp gimp-help-fr gthumb inkscape shutter audacity audacious kdenlive soundconverter easytag transmageddon handbrake ffmpeg gnome-games gnome-chess klavaro remmina remmina-plugin-rdp remmina-plugin-vnc fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine gsfonts-x11 fonts-materialdesignicons-webfont fonts-arkpandora fonts-entypo libreoffice-style-human libreoffice-style-oxygen libreoffice-style-elementary libreoffice-style-sifr tango-icon-theme ttf-mscorefonts-installer grsync timeshift nextcloud-client vlc gnome-tweak-tool gnome-shell gnome-shell-extensions libgoo-canvas-perl chromium-browser pavucontrol calibre p7zip-full p7zip-rar virtualbox-6.0

# Paquets non-libres
apt-get -y install mint-meta-codecs libdvd-pkg gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad msttcorefonts
dpkg-reconfigure libdvd-pkg

# Mise à jour & Nettoyage
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get -y autoremove
apt install -y --fix-broken
apt-get clean

# Ajout de groupe pour Virtualbox

# Recherche de l'utilisateur à personnaliser
if [ "$(who|grep cemea|cut -d" " -f1)" != "cemea" ]; then
        echo "Entrer un nom d'utilisateur existant comme cemea"
        read -r monuser
        while [[ "$(grep $monuser /etc/passwd|cut -d: -f1)" != "$monuser" ]]; do
                echo "Entrez un vrai nom de l'utilisateur à configurer"
                read -r monuser
        done
else
        monuser="cemea"
fi

# echo "Monuser vaut $monuser"

# Privilèges Virtualbox pour cet utilisateur

adduser "$monuser" vboxusers

# Installation de Joplin - a remplacer par Carnet ? https://github.com/PhieF/CarnetDocumentation
# sudo -u cemea wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash

# Ajout Fonds d'écran
mkdir -p /usr/share/backgrounds/cemea
cp -R paquets/backgrounds-cemea /usr/share/backgrounds/cemea
chmod 755 -R /usr/share/backgrounds/cemea

# Finalisation Utilisateur cemea

# sudo -u "$monuser" $(libreoffice paquets/Grammalecte-fr-v0.6.5.oxt) &
# sudo -u "$monuser" $(virtualbox tmpdeb/Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack) &

# sudo -u "$monuser" `firefox paquets/*.xpi`

chown -R $monuser tmpdeb

echo "Ne pas oublier :
- d'ajouter les extensions Firefox dans le dossier paquets/
- d'ajouter le pack de Virtualbox dans tmpdeb
- d'ajouter le Grammalecte à LibreOffice dans tmpdeb
 et d'effacer le dossier tmpdeb avec :
rm -rf tmpdeb"


#!/usr/bin/env bash
# Ajoute Fortigui
apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 23CAE45582EB0928
echo "deb https://apt.iteas.at/iteas focal main" >> /etc/apt/sources.list.d/iteas.list
cp ../paquets/iteas.pref /etc/apt/preferences.d/
apt update && apt install -y openfortigui

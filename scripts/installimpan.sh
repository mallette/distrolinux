#!/usr/bin/env bash
# Ajoute les imprimantes et ses pilotes

# Ajout du filter d'impression et pilote d'impression
cp ../paquets/est6550_Authentication /usr/lib/cups/filter/
chmod +x /usr/lib/cups/filter/est6550_Authentication
mkdir -p /usr/share/cups/model/Toshiba
cp ../paquets/TOSHIBA_* /usr/share/cups/model/Toshiba/
chmod -R 755 /usr/share/cups/model/Toshiba/

systemctl stop cups.service
sleep 2

# On install les imprimantes par défaut
cp ../paquets/printers.conf /etc/cups/
chmod 600 /etc/cups/printers.conf

# On copie les PPD utiles
cp ../paquets/AN-Impr-*.ppd /etc/cups/ppd/

# On intègre le code département
code=""

while [ ${#code} != 5 ]; do
	echo "Entrez le code département (5 chiffres ou 00000) :"
	read code
done

for ppd in $(/bin/ls /etc/cups/ppd/AN-Impr-*.ppd); do
	for i in $(seq 0 4); do
		j=$(($i+1))
		# echo "${code:i:1} -> $j"
		chiffre="${code:i:1}"
		sed -i -e "s/^\*DefaultDCDigit$j:.*/\*DefaultDCDigit$j:\ $chiffre/" "$ppd"
	done
done

chmod 640 /etc/cups/ppd/AN-Impr-*.ppd


systemctl restart cups.service

echo "Merci de patienter 5 secondes le temps de relancer Cups"
echo "1" ; sleep 1
echo "2" ; sleep 1
echo "3" ; sleep 1
echo "4" ; sleep 1
echo "5" ; sleep 1
echo "C'est ok !"


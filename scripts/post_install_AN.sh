#!/usr/bin/env bash
# Meta Post installation pour l'AN

if [ "$(whoami)" != "root" ]; then
        echo "Désolé, vous n'êtes pas roooooot !"
        exit 1
fi

echo "ce script configure plusieurs éléments en post installation automatiquement :
* Ajout du plugin d'inventaire vers GLPI
* Ajout des pilotes d'impression et configuration d'imprimantes
* (Forcer le protocole SMB 2 mini)
* et d'autres bricoles 

Voulez-vous procéder à cette post-installation ? (o/n)"
read reponse

if [[ "$reponse" != [oO] ]]; then
	echo "A bientôt !"
	exit 1;
fi

export DEBIAN_FRONTEND=noninteractive

echo "Timeout Grub 0"
./installgrub.sh

#echo "install de starleaf"
#./installsl.sh

echo "Installation de FortiGui"
./installfg.sh

echo "Installation du GLPI"
./fuglpi.sh

echo "Installation des impressions"
./installimpan.sh

echo "Installation UCS + raccourcis + un profil VPN"
./installucs.sh

#echo "Installation des raccourcis"
#./installrac.sh

echo "Ne pas oublier de :
 - Configurer Nextcloud
 - Tester la connexion au serveur
 - Configurer la connexion Fortigui"

#Ajouter la connexion automatique du user


#!/usr/bin/env bash
# Script d'installation automatique de Fusion Inventory pour l'AN
# support@cemea.asso.fr
# version 2.4.2

if [ $(whoami) != "root" ]; then
        echo "Désolé, vous n'êtes pas roooooot !"
        exit 1
fi

if [ "$1" != "--yes" ]; then
	echo "Etes-vous certain de lancer l'installation avec remontée vers GLPI ? (o/n)"
	read reponse

	if [  "$reponse" != "o" ] &&  [ "$reponse" != "O"  ]; then
	        echo "Ok, tchao !"
	        exit 1;
	fi
fi


apt install -y dmidecode hwdata ucf hdparm perl libuniversal-require-perl libwww-perl libparse-edid-perl libproc-daemon-perl libfile-which-perl libhttp-daemon-perl libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl libxml-xpath-perl libnet-snmp-perl libcrypt-des-perl libnet-nbname-perl libfile-copy-recursive-perl libparallel-forkmanager-perl libfilter-perl libio-pipely-perl libpoe-component-client-ping-perl libpoe-perl libproc-pid-file-perl libjson-perl

rm /etc/fusioninventory/agent.cfg

apt-get update
apt-get -y install fusioninventory-agent fusioninventory-agent-task-network  fusioninventory-agent-task-deploy
apt-get -f install

cp /usr/share/fusioninventory/etc/agent.cfg /etc/fusioninventory/agent.cfg

echo "# Config CEMEA AN Fusion Inventory
server = https://glpi.cemea.org/plugins/fusioninventory/
local = /tmp
tag = AN
tasks = inventory,deploy,inventory
logfile = /var/log/fusioninventory.log " >> /etc/fusioninventory/agent.cfg

sleep 2

# On force la remontée automatique aussitôt
# pkill -USR1 -f -P 1 fusioninventory-agent

fusioninventory-agent --force --no-ssl-check &



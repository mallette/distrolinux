#!/usr/bin/env bash
# Ajoute less raccourcis

# On se connecte au domaine
add-apt-repository -y ppa:univention-dev/ppa
apt update && apt install -y univention-domain-join
univention-domain-join-cli --username admindomain --domain cemea.lan

echo "Entre le nom du login UCS"
read -r login

# On crée le répertoire de l'utilisateur
mkdir -p /home/${login}/.config/gtk-3.0/
# Les " sont importants car le nom de groupe contient un espace
#chown -R "${login}:Domain Users" /home/${login}/

echo "smb://10.1.9.1/partages AN-Partages
smb://10.1.9.1/utilisateurs/${login} AN-Perso" >> /home/${login}/.config/gtk-3.0/bookmarks
#chown "${login}:Domain Users" /home/${login}/.config/gtk-3.0/bookmarks

# On rajoute les droits sudo à l'utilisateur
echo "${login} ALL=(ALL:ALL) ALL
${login} ALL=NOPASSWD:SETENV: /usr/bin/openfortigui --start-vpn *" >> /etc/sudoers.d/univention
# Et les droits pkexec (sudo graphique)
echo "[Configuration]
AdminIdentities=unix-user:${login}" >> /etc/polkit-1/localauthority.conf.d/65-cemea.conf

# On rajoute une conf basique pour le VPN Cemea (on le fait ici car on a besoin que l'utilisateur existe)
mkdir -p /home/${login}/.openfortigui/vpnprofiles
cp ../paquets/VPNCEMEA.conf /home/${login}/.openfortigui/vpnprofiles/
sed -i -e "s/^username=/username=${login}/" /home/${login}/.openfortigui/vpnprofiles/VPNCEMEA.conf

chown -R "${login}:Domain Users" /home/${login}/

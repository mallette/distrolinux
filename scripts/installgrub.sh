#!/usr/bin/env bash
# Ajoute le menu caché de GRub

if [  $(grep "GRUB_TIMEOUT=" /etc/default/grub) ];then
	sed -i -e "s/^.*GRUB_TIMEOUT=.*$/GRUB_TIMEOUT=0/" /etc/default/grub
fi

if [  $(grep "GRUB_DEFAULT=" /etc/default/grub) ];then
	sed -i -e "s/^.*GRUB_DEFAULT=.*$/GRUB_DEFAULT=0/" /etc/default/grub
fi

update-grub


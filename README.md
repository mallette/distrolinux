# Distribution GNU/Linux CEMEA

**Objectifs**

Proposer une distribution Linux clef en main aux formateurs, animateurs, stagiaires contenant :
* Logiciels choisis
* Documentation
* Ressources libres

Voir la documentation des prérecquis :
https://ladoc.cemea.org/doku.php/technique:distriblinuxcemea

## Scripts de mise à jour CEMEA

**Objectifs**

### Linux Mint

- [ ] 20 -> 20.3

- [ ] 20.3 -> 21 (dist-upgrade)

- [ ] 21 -> 21.3

- [ ] 21.3 -> 22 (dist-upgrade)

- [ ] 22 -> 22.3

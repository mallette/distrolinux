#!/usr/bin/env bash

# Création et mise à jour Linux Ubuntu pour CEMEA AN

mkdir paquets
cd paquets
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas-common_1.0.0-1_all.deb
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas3_1.0.0-1_amd64.deb
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb
wget https://apt.iteas.at/iteas/pool/main/o/openfortigui/openfortigui_0.7.2-3_bionic_amd64.deb
wget https://code-industry.net/public/master-pdf-editor-5.3.16_qt5.amd64.deb
wget https://github.com/torakiki/pdfsam/releases/download/v4.0.1/pdfsam_4.0.1-1_all.deb
wget https://download.virtualbox.org/virtualbox/6.0.4/Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack
cd ..


# add-apt-repository -y ppa:phoerious/keepassxc # dépot keepassxc
add-apt-repository -y ppa:libreoffice/ppa
add-apt-repository -y ppa:teejee2008/ppa # dépot timeshift
add-apt-repository -y ppa:nextcloud-devs/client
# add-apt-repository -y ppa:shutter/ppa

dpkg -i paquets/*.deb
apt-get -y install --fix-broken

echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bionic contrib" > /etc/apt/sources.list.d/virtualbox.list
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -


apt-get -y purge firefox-locale-de firefox-locale-en firefox-locale-es firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans thunderbird-locale-de thunderbird-locale-en thunderbird-locale-en-gb thunderbird-locale-en-us thunderbird-locale-es thunderbird-locale-es-ar thunderbird-locale-es-es thunderbird-locale-it thunderbird-locale-pt thunderbird-locale-pt-br thunderbird-locale-pt-pt thunderbird-locale-ru thunderbird-locale-zh-cn thunderbird-locale-zh-hans thunderbird-locale-zh-hant thunderbird-locale-zh-tw hunspell-* hyphen-* mythes-* libreoffice-help-de libreoffice-help-en-gb libreoffice-help-en-us libreoffice-help-es libreoffice-help-it libreoffice-help-pt libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw  libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw
apt-get -y purge fonts-tlwg-* fonts-lohit-* fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak-* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-takao-pgothic fonts-takao-gothic fonts-telu* fonts-khmeros-core* fonts-tibetan-machine* fonts-deva-extra* fonts-kacst* fonts-kacst-one* fonts-kalapi* fonts-nakula* fonts-nanum* fonts-navilu* fonts-noto* fonts-noto-cjk* fonts-noto-hinted* fonts-noto-mono* fonts-noto-unhinted* fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lao fonts-lklug-sinhala fonts-wqy-microhei fonts-beng-extra
apt-get -y update
apt-get -y install git curl mythes-fr hyphen-fr hunspell-fr wfrench manpages-fr manpages-fr-extra doc-linux-fr-text fortunes-fr p7zip geany geany-plugins fslint vim grsync meld tomboy keepassxc bleachbit thunderbird thunderbird-locale-fr firefox firefox-locale-fr pidgin pwgen pass typecatcher gparted synaptic whois sysinfo pidgin freeplane scribus dia dia-shapes xournal pinta gimp gimp-help-fr gthumb inkscape shutter audacity audacious kdenlive soundconverter easytag transmageddon handbrake ffmpeg gnome-games gnome-chess klavaro remmina remmina-plugin-rdp remmina-plugin-vnc fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine gsfonts-x11 fonts-materialdesignicons-webfont fonts-arkpandora fonts-entypo libreoffice-style-human libreoffice-style-oxygen libreoffice-style-elementary libreoffice-style-sifr libreoffice-style-hicontrast tango-icon-theme ttf-mscorefonts-installer grsync timeshift nextcloud-client vlc gnome-tweak-tool gnome-shell gnome-shell-extensions libgoo-canvas-perl chromium-browser pavucontrol calibre p7zip-full p7zip-rar virtualbox-6.0

# Paquets non-libres
apt-get -y install ubuntu-restricted-extras libdvd-pkg gstreamer1.0-plugins-ugly gstreamer1.0-plugins-bad msttcorefonts
dpkg-reconfigure libdvd-pkg

# Mise à jour & Nettoyage
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install -y --fix-broken


# Ajout de groupe pour Virtualbox
adduser cemea vboxusers

# Installation de Joplin
sudo -u cemea 'wget -O - https://raw.githubusercontent.com/laurent22/joplin/master/Joplin_install_and_update.sh | bash'

# Ajout du filter d'impression
cp paquets/est6550_Authentication /usr/lib/cups/filter/
chmod +x /usr/lib/cups/filter/est6550_Authentication

# Ajout de l'antivirus à copier dans un dossier Linux puis installer
apt --yes install libc6:i386

# Ajout Fonds d'écran
mkdir /usr/share/backgrounds/cemea
cp -R scripts/backgrounds-cemea/* /usr/share/backgrounds/cemea/
chmod 755 -R /usr/share/backgrounds/cemea

# Installation de GLPI Fusion Inventory
chmod +x paquets/fuglpi.sh && ./fuglpi.sh

#suppression langue en
apt-get --yes purge language-pack-en language-pack-en-base language-pack-gnome-en language-pack-gnome-en-base

# Finalisation Utilisateur cemea
sudo -u cemea `libreoffice paquets/Grammalecte-fr-v0.6.5.oxt`
sudo -u cemea `virtualbox paquets/Oracle_VM_VirtualBox_Extension_Pack-6.0.4.vbox-extpack`
sudo -u cemea `firefox paquets/*.xpi`


apt-get -y update
apt-get -y upgrade
apt-get -y dist-upgrade
apt-get install -y --fix-broken
apt-get --yes clean
apt-get --yes autoremove

# Post install
echo "Reste à installer Antivirus et imprimantes"
